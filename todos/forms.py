from django import forms

from todos.models import TodoList, TodoItem

class TodoForm(forms.ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name"
        ]


class InputForm(forms.ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            "task",
            "due_date",
            "is_completed",
            "todolist"
        ]
