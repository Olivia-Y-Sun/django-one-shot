
from django.contrib import admin

from todos.models import TodoList, TodoItem

# Register your models here.

admin.site.register(TodoList)
admin.site.register(TodoItem)

# from django.contrib import admin
# from .models import Project, Client, About

# admin.register(Project, Client, About)(admin.ModelAdmin)